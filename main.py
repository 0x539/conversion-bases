#!/usr/bin/python3

"""
Copyright (C) 2015 Corentin Bocquillon <corentin+framagit@nybble.fr>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

def nombreVersSymbole(nombre):
    dict = {}

    for i in range(10):
        dict[str(i)] = str(i)

    for i in range(6):
        dict[str(10+i)] = chr(ord('A') + i)

    return dict[str(nombre)]

def symboleVersNombre(symbole):
    dict = {}

    for i in range(10):
        dict[str(i)] = i

    for i in range(6):
        dict[chr(ord('A') + i)] = 10+i
        
    return dict[symbole]

def decimalVersAutreBase(base, nombre):
    nombreConverti = ""

    while nombre > base-1:
        nombreConverti = nombreVersSymbole(nombre % base) + nombreConverti
        nombre //= base

    nombreConverti = str(nombre) + nombreConverti
    return nombreConverti

def versDecimal(base, nombre):
    nombreEnDecimal = 0

    for i in range(len(nombre)):
        nombreEnDecimal += int(symboleVersNombre(nombre[len(nombre)-1-i])) * base ** i

    return nombreEnDecimal

baseNombreAConvertir = int(input("Quel est la base du nombre à convertir?: "))
nombreAConvertir = input("Quel est le nombre à convertir?: ")
baseNombreConverti = int(input("En quelle base voulez-vous convertir ce nombre ?: "))

print(decimalVersAutreBase(baseNombreConverti, versDecimal(baseNombreAConvertir, nombreAConvertir)))
